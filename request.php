<?php 

include 'header.php'; 

if (!isset($_SESSION['username'])) {
  "Location: index.php";
}

?>

<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="style/styleprofile.css">
<meta charset="utf-8">
<meta name="description" content="">
<meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
<meta name="generator" content="Hugo 0.101.0">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="canonical" href="https://getbootstrap.com/docs/5.2/examples/album/">
<link href="style/bootstrap.min.css" rel="stylesheet">
<style>
    body{
        background-color:white;
    }
    
    h1{
        font-size:2.1rem;
        line-height:1.4;
        letter-spacing:0.5rem;
        text-align:center;
        color:#E33467;
        margin-top:25px;
    }

    #myBtn {
        display: none;
        position: fixed;
        bottom: 20px;
        right: 30px;
        z-index: 99;
        font-size: 18px;
        border: none;
        outline: none;
        background-color: #E33467;
        color: white;
        cursor: pointer;
        padding: 15px;
        border-radius: 4px;
    }

    #myBtn:hover {
        background-color: #555;
    }

    .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

    @media (min-width: 768px) {
      .bd-placeholder-img-lg {
        font-size: 3.5rem;
      }
    }

    .b-example-divider {
      height: 3rem;
      background-color: rgba(0, 0, 0, .1);
      border: solid rgba(0, 0, 0, .15);
      border-width: 1px 0;
      box-shadow: inset 0 .5em 1.5em rgba(0, 0, 0, .1), inset 0 .125em .5em rgba(0, 0, 0, .15);
    }

    .b-example-vr {
      flex-shrink: 0;
      width: 1.5rem;
      height: 100vh;
    }

    .bi {
      vertical-align: -.125em;
      fill: currentColor;
    }

    .nav-scroller {
      position: relative;
      z-index: 2;
      height: 2.75rem;
      overflow-y: hidden;
    }

    .nav-scroller .nav {
      display: flex;
      flex-wrap: nowrap;
      padding-bottom: 1rem;
      margin-top: -1px;
      overflow-x: auto;
      text-align: center;
      white-space: nowrap;
      -webkit-overflow-scrolling: touch;
    }

</style>
</head>
<main>
<body>


  <a href="https://wa.me/6285865705342">
    <button class="btn-floating whatsapp">
      <img src="img/profile/whatsapp.png" alt="WhatsApp">
      <span>MULTICREATIVE</span>
    </button>
  </a>

  <a href="https://www.instagram.com/multicreativee/">
    <button class="btn-floating instagram">
      <img src="img/profile/instagram.png" alt="Instagram">
      <span>MULTICREATIVE</span>
    </button>
  </a>
<button onclick="topFunction()" id="myBtn" title="Go to top">Top</button>

<script>

let mybutton = document.getElementById("myBtn");

window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    mybutton.style.display = "block";
  } else {
    mybutton.style.display = "none";
  }
}

function topFunction() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}
</script>

<section class="py-5 text-center container">
    <div class="row py-lg-5">
      <div class="col-lg-6 col-md-8 mx-auto">
        <h1 class="fw-light">REQUEST PESANAN</h1>
        <p class="lead text-muted">Anda ingin secara spesifik memesan layanan kami? Ingin menambah Add-on? Silahkan tulis dan salin pesanan anda, kemudian kirimkan kepada kami melalui kontak yang tersedia.</p>
      </div>
    </div>
  </section>
    
    <form action="result.php" method="post" class="mx-auto">
        <textarea id="editor" name="konten">Fill here</textarea>
    
        <!--<input type="submit" value="Simpan">-->
        <a href="https://www.instagram.com/multicreativee/" class="btn my-2 d-flex justify-content-center" style="background-color: #E33467; color:white">KIRIM</a>
    </form>
    
    <script src="https://cdn.ckeditor.com/ckeditor5/35.3.2/classic/ckeditor.js"></script>
    <script>
    ClassicEditor
        .create(document.querySelector('#editor'))
        .catch(error => {
            console.error(error);
        });
    </script>
    
</body>
</main>
</html>

<?php include 'footer.php'; ?>