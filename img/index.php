<?php 

include 'header.php';

error_reporting(0);

if (isset($_SESSION['username'])) {
    header("Location: welcome.php");
}

if (isset($_POST['submit'])) {
	$email = $_POST['email'];
	$password = md5($_POST['password']);

	$sql = "SELECT * FROM users WHERE email='$email' AND password='$password'";
	$result = mysqli_query($conn, $sql);
	if ($result->num_rows > 0) {
		$row = mysqli_fetch_assoc($result);
		$_SESSION['username'] = $row['username'];
		header("Location: welcome.php");
	} else {
		echo "<script>alert('Woops! Email or Password is Wrong.')</script>";
	}
}

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

	<link rel="stylesheet" type="text/css" href="#">

</head>
<body>
	<div class="container" style="margin:0 auto;text-align:center">
		<form action="" method="POST" class="login-email">
			<p class="login-text" style="font-size: 2rem; font-weight: 800; color:#E33467">Login</p>
			<div class="input-group" style="margin-bottom:10px; justify-content: center">
				<input type="email" placeholder="Email" name="email" value="<?php echo $email; ?>" required>
			</div>
			<div class="input-group" style="justify-content: center">
				<input type="password" placeholder="Password" name="password" value="<?php echo $_POST['password']; ?>" required>
			</div>
			<div class="input-group" style="justify-content: center">
				<button name="submit" class="btn" style="color:white; background-color:#E33467; margin-top:20px">Login</button>
			</div>
			<p class="login-register-text">Tidak punya akun? <a href="register.php" style="color:#E33467">Daftar Disini</a>.</p>
		</form>
	</div>
</body>
</html>

<?php include 'footer.php'; ?>