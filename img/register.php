<?php 

include 'header.php';

error_reporting(0);


if (isset($_SESSION['username'])) {
    header("Location: index.php");
}

if (isset($_POST['submit'])) {
	$username = $_POST['username'];
	$email = $_POST['email'];
	$password = md5($_POST['password']);
	$cpassword = md5($_POST['cpassword']);

	if ($password == $cpassword) {
		$sql = "SELECT * FROM users WHERE email='$email'";
		$result = mysqli_query($conn, $sql);
		if (!$result->num_rows > 0) {
			$sql = "INSERT INTO users (username, email, password)
					VALUES ('$username', '$email', '$password')";
			$result = mysqli_query($conn, $sql);
			if ($result) {
				echo "<script>alert('Wow! Pendaftaran selesai.')</script>";
				$username = "";
				$email = "";
				$_POST['password'] = "";
				$_POST['cpassword'] = "";
			} else {
				echo "<script>alert('Woops! Ada yang salah.')</script>";
			}
		} else {
			echo "<script>alert('Woops! Email sudah terdaftar.')</script>";
		}
		
	} else {
		echo "<script>alert('Password tidak sesuai.')</script>";
	}
}

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

	<link rel="stylesheet" type="text/css" href="style.css">

	<title>Formulir Pendaftaran</title>
</head>
<body>
	<div class="container" style="margin:0 auto;text-align:center">
		<form action="" method="POST" class="login-email">
            <p class="login-text" style="font-size: 2rem; font-weight: 800; color:#E33467">Daftar</p>
			<div class="input-group" style="margin-bottom:10px; justify-content: center">
				<input type="text" placeholder="Username" name="username" value="<?php echo $username; ?>" required>
			</div>
			<div class="input-group" style="margin-bottom:10px; justify-content: center">
				<input type="email" placeholder="Email" name="email" value="<?php echo $email; ?>" required>
			</div>
			<div class="input-group" style="margin-bottom:10px; justify-content: center">
				<input type="password" placeholder="Password" name="password" value="<?php echo $_POST['password']; ?>" required>
            </div>
            <div class="input-group" style="justify-content: center">
				<input type="password" placeholder="Confirm Password" name="cpassword" value="<?php echo $_POST['cpassword']; ?>" required>
			</div>
			<div class="input-group" style="justify-content: center">
				<button name="submit" class="btn" style="color:white; background-color:#E33467; margin-top:20px">Daftar</button>
			</div>
			<p class="login-register-text">Sudah punya akun? <a href="index.php" style="color:#E33467">Masuk Disini</a>.</p>
		</form>
	</div>
</body>
</html>


<?php include 'footer.php'; ?>